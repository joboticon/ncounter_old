// ResetDialog.qml
//
// This file is part of the nCounter application.
//
// Copyright (c) 2017
//
// Maintained by Joe (@exar_kun) <joe@ubports.com>
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.6
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

// i18n.tr("Popup for reset case.")
Dialog {
    id: reset
    text: i18n.tr("Clear ALL settings? Cannot be undone.")    

    signal dialogAccepted
    signal dialogCanceled


    // i18n.tr("Reset button resets all settings.")
    Button {
        id: okButton
        
        text: i18n.tr("Reset")
        color: UbuntuColors.green
        onClicked:  {
            dialogAccepted();
            settings.myEvent = "";
            settings.myDate = new Date();
            eventDate.date = new Date();
            settings.myReport = 0;
            settings.myLast = 0;
            settings.lastEvent = 0;
            settings.restarts = 0;
            PopupUtils.close(reset);
        }
    }

    // i18n.tr("Cancel button closes popup and does nothing.")
    Button {
        id: cancelButton
        text: i18n.tr("Cancel")
        color: UbuntuColors.red
        onClicked: {
            dialogCanceled();
            PopupUtils.close(reset);
        }
    }
}
