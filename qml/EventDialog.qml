// EventDiaglog.qml
//
// This file is part of the nCounter application.
//
// Copyright (c) 2017
//
// Maintained by Joe (@exar_kun) <joe@ubports.com>
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.6
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

// i18n.tr("Popup for empty event case.")
Dialog {
    id: event
    text: i18n.tr("Please enter an event.")    

    signal dialogAccepted


    // i18n.tr("Button to close popup.")
    Button {
        id: okButton    
        text: i18n.tr("OK")
        color: UbuntuColors.green
        onClicked:  {
            dialogAccepted();
            PopupUtils.close(event);
        }
    }
}
