function calcThat(a,b,c,x,y,z)
{
   // event date variables
   var eyear = String(a)
   var emonth = String(b)
   var eday = String(c)

   if (emonth < 10)
   {
       emonth = "0" + emonth
   }
   if (eday < 10)
   {
       eday = "0" + eday
   }
   
   
   // today's date variables
   var tyear = String(x)
   var tmonth = String(y)
   var tday = String(z)
   
   if (tmonth < 10)
   {
       tmonth = "0" + tmonth
   }
   if (tday < 10)
   {
       tday = "0" + tday
   }
   // add them up and make them integers
   var edate = eyear + emonth + eday
   var tdate = tyear + tmonth + tday
   var event = parseInt(edate)
   var today = parseInt(tdate)

   // return the difference: negative if event in future, positive if event in past
   return today - event
}
